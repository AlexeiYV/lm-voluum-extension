export function Renderer(tableHeaderRefSelector, tableBodyRefSelector, tableFooterRefSelector) {
  this.tableHeaderRefSelector = tableHeaderRefSelector;
  this.tableBodyRefSelector = tableBodyRefSelector;
  this.tableFooterRefSelector = tableFooterRefSelector;
  this.sortedBy = null;
  this.sortedASC = false;
  this.rowsDataMap = [];
  this.createdElements = {};
}
Renderer.prototype = {
  sanitizeMap: {
    clicks: {
      callback: parseInt,
      rgxp: /,/g
    },
    visits: {
      callback: parseInt,
      rgxp: /,/g
    },
    '2dClicksSub': {
      callback: parseFloat,
    },
    '10SecCost': {
      callback: parseFloat,
    },
    customConversions1: {
      callback: parseInt,
    },
    customConversions2: {
      callback: parseInt,
    },
    customConversions11: {
      callback: parseInt,
    },
    customConversions13: {
      callback: parseInt,
      rgxp: /,/g
    },
    customConversions17: {
      callback: parseInt,
      rgxp: /,/g
    },
    customRevenue17: {
      callback: parseFloat,
      rgxp: /\$|,/g
    },
    customRevenue18: {
      callback: parseFloat,
      rgxp: /\$|,/g
    },
    customRevenue19: {
      callback: parseFloat,
      rgxp: /\$|,/g
    },
    customRevenue20: {
      callback: parseFloat,
      rgxp: /\$|,/g
    },
    cost: {
      callback: parseFloat,
      rgxp: /\(|,|\$/g
    },
    profit: {
      callback: parseFloat,
      rgxp: /\)|,|\$/g,
      keepNegative: true
    },
    revenue: {
      callback: parseFloat,
      rgxp: /\$|,/g
    },
    cpv: {
      callback: parseFloat,
      rgxp: /\(|,|\$/g
    }
  },
  init: function () {
    this.bindElements();
    this.prepareRowProperties();
    this.prepareRowsDataMap();
    this.recalculate();
  },

  bindElements: function () {
    this.tableHeaderRef = document.querySelector(this.tableHeaderRefSelector);
    this.tableBodyRef = document.querySelector(this.tableBodyRefSelector);
    this.tableFooterRef = document.querySelector(this.tableFooterRefSelector);
    this.rowsMap = this.tableBodyRef.querySelectorAll('.rows .row');
  },

  addColumn: function (columnTitle, beforeSelector, formula, suffix, addTotal) {

    /* clearing columns from previous calls */
    this.createdElements[columnTitle] = this.createdElements[columnTitle] || [];
    for (var i = 0; i < this.createdElements[columnTitle].length; i++) {
      this.createdElements[columnTitle][i].remove();
    }
    this.createdElements[columnTitle] = [];


    var headBeforeSelectorElement = this.tableHeaderRef.querySelector('td' + beforeSelector);
    var headTd = document.createElement('td');
    this.createdElements[columnTitle].push(headTd);
    suffix = suffix || '';
    headTd.innerHTML = `<div class="grabber"></div><span class="text"><span>${columnTitle}</span></span>`;
    headTd.classList.add('column');
    headTd.dataset['key'] = columnTitle;
    headTd.addEventListener('click', (e) => {
      if (!e.target.classList.contains('grabber')) {
        this.sortBy(columnTitle);
      }
    });
    headBeforeSelectorElement.after(headTd);

    var colgroupElement = this.tableHeaderRef.querySelector('colgroup');
    var bodyColgroupElement = this.tableBodyRef.querySelector('colgroup');
    var footerColgroupElement = this.tableFooterRef.querySelector('colgroup');
    var footerOverallRow = this.tableFooterRef.querySelector('.rows tr');

    var colItemElement = document.createElement('col');
    this.createdElements[columnTitle].push(colItemElement);
    colItemElement.classList.add(columnTitle, 'col-group', 'extension-custom-column');
    colItemElement.dataset['key'] = columnTitle;
    colItemElement.style.width = '60px';

    var overallRowTd = document.createElement('td');
    overallRowTd.classList.add('column');
    overallRowTd.classList.add(columnTitle);


    colgroupElement.querySelector('col' + beforeSelector).after(colItemElement);
    bodyColgroupElement.querySelector('col' + beforeSelector).after(colItemElement.cloneNode(true));


    var totalValue = 0;
    var i = 0;

    for (var row of this.rowsDataMap) {
      var tdValue = formula(row);
      tdValue = this.isComputable(tdValue) ? tdValue : 'N/A';
      row[columnTitle] = tdValue;
      var bodyTd = document.createElement('td');
      this.createdElements[columnTitle].push(bodyTd);
      bodyTd.classList.add('cell');
      bodyTd.innerHTML = `<span class="text"><span class="cell-wrapper">${tdValue}${tdValue != 'N/A' ? suffix : ''}</span></span>`;
      this.rowsMap[i].querySelector('td' + beforeSelector).after(bodyTd);

      // total value
      if (addTotal && tdValue !== 'N/A') {
        totalValue += +tdValue;
      }

      i++;
    }

    if (addTotal && typeof totalValue != 'object') {
      overallRowTd.innerHTML = '<span class="text">' + totalValue.toFixed(2) + '</span>'
    }
    footerColgroupElement.querySelector('col' + beforeSelector).after(colItemElement.cloneNode(true));
    footerOverallRow.querySelector('.column' + beforeSelector).after(overallRowTd);
  },
  sortBy: function (sortByProperty) {
    var self = this;
    var updatedRowsMap = Array.from(this.rowsMap).sort(function (a, b) {
      var output;
      var aVal = self.rowsDataMap.find(el => el.id == a.id)[sortByProperty];
      var bVal = self.rowsDataMap.find(el => el.id == b.id)[sortByProperty];

      console.log(aVal, bVal);

      if (isNaN(aVal) || isNaN(bVal)) return 1;
      var output = aVal - bVal;
      if (self.sortedBy == sortByProperty && self.sortedASC) output = (output < 0) ? Math.abs(output) : output * -1;
      return output;
    });
    this.rowsMap = updatedRowsMap;
    this.reRender(updatedRowsMap);
    this.sortedBy = sortByProperty;
    this.sortedASC = !this.sortedASC;
  },

  reRender(newRowsMap) {
    var rowsElement = this.tableBodyRef.querySelector('.rows');
    rowsElement.innerHTML = null;
    var beforeRowsOffset = document.createElement('tr');
    beforeRowsOffset.classList.add('before-rows-offset');
    rowsElement.append(beforeRowsOffset);
    var afterRowsOffset = document.createElement('tr');
    afterRowsOffset.classList.add('after-rows-offset');
    for (var row of newRowsMap) {
      rowsElement.append(row);
    }
    rowsElement.append(afterRowsOffset);

  },

  insertTdAfter: function (name, previousNode, value, classes) {
    classes = classes || [];
    var _className = classes.join(' ');
    var newNode = document.createElement('td');
    newNode.className = _className;
    newNode.innerHTML = value;
    previousNode.after(newNode);
  },

  prepareRowProperties: function () {
    this.rowPropertiesMap = Array.from(this.tableHeaderRef.querySelectorAll('td'))
      .slice(2)
      .map(function (el) { return el.dataset.key })
  },

  prepareRowsDataMap: function () {
    var self = this;
    this.rowsDataMap = Array.from(this.tableBodyRef.querySelectorAll('.row')).map(function (el) {
      var newRow = {};
      newRow.id = el.id;

      for (var property of self.rowPropertiesMap) {
        var cellElWrapper = el.getElementsByClassName(property);
        if (cellElWrapper.length) {
          var cellEl = cellElWrapper[0].querySelector('.cell-wrapper');
          if (!cellEl) console.log('element for' + property + 'not found');
          if (cellEl.children.length) {
            cellEl = cellEl.querySelector('span')
          }
          var rawValue = cellEl ? cellEl.innerHTML.trim() : '0';
          if (self.sanitizeMap[property]) {
            if (self.sanitizeMap[property].keepNegative) rawValue = rawValue.replace(/\(/g, '-');
            rawValue = rawValue.replace(self.sanitizeMap[property].rgxp, '');
            rawValue = self.sanitizeMap[property].callback(rawValue);
          }
          newRow[property] = rawValue;
        }
      }
      return newRow;
    });
  },

  recalculate: function () {
    this.rowsMap[0].click();
  },

  isComputable: function (tdValue) {
    return !(tdValue == 'NaN' || tdValue == 'Infinity' || tdValue == '-Infinity');
  }
}