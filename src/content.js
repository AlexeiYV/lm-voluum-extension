import { Renderer } from './functions';

var renderer;

chrome.runtime.onMessage.addListener(gotMessage);

var adtSPValue;
var columnVisibilityMap = {
  'ss': true, 
  'pps': true, 
  'cps': true, 
  'lps-aff': true,
  'oi-rate': true,
  '30d-profit': true,
  '30d-roi': true,
  'fcpv': true,
  'pjlps': true
};

initSettings(function(result) {
  adtSPValue = Number(result['adt-sp-value']) || 0.5;
  Object.keys(columnVisibilityMap).map(function(column) {
    columnVisibilityMap[column] = typeof result[column + '-column-visibility'] === 'undefined' ? columnVisibilityMap[column] : result[column + '-column-visibility'];
  });
});



chrome.storage.onChanged.addListener(function (changes, namespace) {
  for (var key in changes) {
    var storageChange = changes[key];

    if (key === 'adt-sp-value') {
      adtSPValue = storageChange.newValue
    }

    var columnName = null;

    if (key.indexOf('-column-visibility') != -1) {
      columnName = key.replace('-column-visibility', '');
    }

    
    if (Object.keys(columnVisibilityMap).includes(columnName)) {
      columnVisibilityMap[columnName] = storageChange.newValue;
    }
  }
});


function gotMessage(message, sender, sendResponse) {
  if (message.event == 'click') {
    if (!renderer) {
      renderer = new Renderer(
        '.table-header',
        '.table-body',
        '.table-footer'
      );
    }

    renderer.init();

    if (message.type && message.type == 'jbm-av-flow') {
      var networkAvg = message.data.networkAvg;
      var clixSub = message.data.clixSub;
      


      renderer.addColumn('JBM-AV-Flow', '.clicks', (row) => {
        console.log(row['2dClicksSub']);
        if (row['2dClicksSub'] == 0) return 'N/A';
        return (((3 * (row['2dClicksSub'] * (networkAvg / clixSub))) - ((row['customConversions1'] * (row['10SecCost'] - (2 * row['cpv']) / 0.16)) / row['customConversions17'])) * (1 / (row['cost'] / row['customConversions17']))).toFixed(3);
      });
      return;
    }

    if (columnVisibilityMap['ss']) {
      renderer.addColumn('SS', '.ecpc', (row) => (row['customConversions17'] / row['cost']).toFixed(3));
    }
    if (columnVisibilityMap['pps']) {
      renderer.addColumn('PPS', '.ecpc', (row) => ((row['revenue'] - row['cost']) / row['customConversions17']).toFixed(3));
    }
    if (columnVisibilityMap['cps']) {
      renderer.addColumn('CPS', '.clicks', (row) => (row['cost'] / row['customConversions17']).toFixed(3));
    }
    if (columnVisibilityMap['lps-aff']) {
      renderer.addColumn('LPS-AFF', '.clicks', (row) => {
        if (row['revenue'] == 0) return ((row['cost'] / row['customConversions17']) * -1).toFixed(3);
        return (((row['revenue'] - row['customRevenue18']) - row['cost']) / row['customConversions17']).toFixed(3);
      });
    }
    if (columnVisibilityMap['oi-rate']) {
      renderer.addColumn('OI-rate', '.clicks', (row) => {
        if (row['customConversions17'] == 0 || row['visis'] == 0) return 'N/A';
        return ((row['customConversions17'] / row['visits']) * 100).toFixed(0) + '%';
      });
    }
    if (columnVisibilityMap['30d-profit']) {
      renderer.addColumn('30d-Profit', '.ecpc', (row) => (row['revenue'] - row['customRevenue18'] - row['cost'] + row['customRevenue19']).toFixed(2), false, true);
    }
    if (columnVisibilityMap['30d-roi']) {
      renderer.addColumn('30d-ROI', '.ecpc', (row) => {
        if (row['cost'] == 0) return 'N/A';
        return (((row['revenue'] - row['customRevenue18'] + row['customRevenue19'] - row['cost']) / row['cost']) * 100).toFixed(2) + '%';
      });
    }
    if (columnVisibilityMap['fcpv']) {
      renderer.addColumn('FCPV', '.clicks', (row) => {
        if (row['customRevenue20'] == 0 || row['visits'] == 0) return 'N/A';
        return (row['customRevenue20'] / row['visits']).toFixed(3);
      }, false, true);
    }
    if (columnVisibilityMap['pjlps']) {
      renderer.addColumn('PJLPS', '.clicks', (row) => {
        if (row['customConversions13'] == 0 || row['customConversions13'] == 0) return 'N/A';
        return ((row['cost'] - (row['customConversions13'] * adtSPValue)) / row['customConversions17']).toFixed(3);
      });
    }
    // renderer.addColumn('3d-Profit', '.clicks', (row) => (row['revenue'] - row['customRevenue18'] - row['cost'] + row['customRevenue20']).toFixed(2));
    // renderer.addColumn('3d-ROI', '.clicks', (row) => {
    //   if (row['cost'] == 0) return 'N/A';
    //   return (((row['revenue'] - row['customRevenue18'] + row['customRevenue20'] - row['cost']) / row['cost']) * 100).toFixed(2) + '%';
    // });

    console.log('finished adding columns');
  }
}

function initSettings(callback) {
  getValueFromStorage([
      'adt-sp-value', 
      'ss-column-visibility', 
      'pps-column-visibility', 
      'cps-column-visibility', 
      'lps-aff-column-visibility',
      'oi-rate-column-visibility',
      '30d-profit-column-visibility',
      '30d-roi-column-visibility',
      'fcpv-column-visibility',
      'pjlps-column-visibility'
  ], callback);
}

function getValueFromStorage(settings, callback) {
  chrome.storage.sync.get(settings, callback);
}