window.onload = function () {
    var adtSPValueInput = document.querySelector(".js-adt-sp-value");
    var adtSPValueButton = document.querySelector(".js-adt-sp-value-button");
    var runExtButton = document.querySelector('.js-run-vlm-ext');
    var columnsVisibilityButtons = document.querySelectorAll('.js-cc-visibility .button');
    var jbmAVFlowButton = document.querySelector('.js-jbm-av-flow-btn');
    var jbmCountrySelect = document.querySelector('.js-jbm-country');
    var jbmNetworkSelect = document.querySelector('.js-jbm-network');

    columnVisibilityMap = {
        'ss': true,
        'pps': true,
        'cps': true,
        'lps-aff': true,
        'oi-rate': true,
        '30d-profit': true,
        '30d-roi': true,
        'fcpv': true,
        'pjlps': true
    };

    var jbmCountry = jbmCountrySelect.value;
    var jbmNetwork = jbmNetworkSelect.value;

    jbmCountrySelect.addEventListener('change', function(e) {
        jbmCountry = e.target.value;
    });

    jbmNetworkSelect.addEventListener('change', function(e) {
        jbmNetwork = e.target.value;
    });

    initSettings(function (result) {
        adtSPValueInput.value = result['adt-sp-value'] || 0.5;

        Object.keys(columnVisibilityMap).map(function (column) {
            columnVisibilityMap[column] = typeof result[column + '-column-visibility'] === 'undefined' ? columnVisibilityMap[column] : result[column + '-column-visibility'];
        });

        initColumnVisibilitySettings(columnVisibilityMap, columnsVisibilityButtons);
    });

    adtSPValueButton.addEventListener('click', function (e) {
        saveToStorage(adtSPValueInput.value, 'adt-sp-value', function () {
            console.log('saved');
            getValueFromStorage(['adt-sp-value'], function (result) {
                adtSPValueInput.value = result['adt-sp-value'];
            });
        });
    });

    runExtButton.addEventListener('click', function (e) {
        chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
            chrome.tabs.sendMessage(tabs[0].id, { event: "click" });
        });
    });



    for (var button of columnsVisibilityButtons) {
        button.addEventListener('click', function (e) {
            var columnName = e.target.dataset.column;
            var currentSettingValue = columnVisibilityMap[columnName];
            saveToStorage(!currentSettingValue, columnName + '-column-visibility', function () {
                e.target.classList.toggle('is-info');
            });
        });
    }

    chrome.storage.onChanged.addListener(function (changes, namespace) {
        for (var key in changes) {
            var storageChange = changes[key];

            var columnName = null;

            if (key.indexOf('-column-visibility') != -1) {
                columnName = key.replace('-column-visibility', '');
            }

            if (Object.keys(columnVisibilityMap).includes(columnName)) {
                columnVisibilityMap[columnName] = storageChange.newValue;
            }
        }
    });

    jbmAVFlowButton.addEventListener('click', function (e) {
        var self = e.target;
        self.classList.toggle('is-loading');
        axios.post('https://frontendtools.site/vlm-ext/api/1.0/jbm-av-flow-data', {
            geo: jbmCountry,
            network: jbmNetwork
        }).then(function (result) {
            self.classList.remove('is-loading');
            chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
                chrome.tabs.sendMessage(tabs[0].id, { event: "click", type: 'jbm-av-flow', data: result.data });
            });
        });
    });
}

function saveToStorage(value, settingName, callback) {
    if (typeof value === 'undefined') {
        return;
    }

    var settingsObject = {}
    settingsObject[settingName] = value;
    chrome.storage.sync.set(settingsObject, callback);
}

function getValueFromStorage(settings, callback) {
    chrome.storage.sync.get(settings, callback);
}

function initSettings(callback) {
    getValueFromStorage([
        'adt-sp-value',
        'ss-column-visibility',
        'pps-column-visibility',
        'cps-column-visibility',
        'lps-aff-column-visibility',
        'oi-rate-column-visibility',
        '30d-profit-column-visibility',
        '30d-roi-column-visibility',
        'fcpv-column-visibility',
        'pjlps-column-visibility'
    ], callback);
}

function initColumnVisibilitySettings(columnVisibilityMap, columnVisibilitySwitchers) {
    columnVisibilitySwitchers = Array.from(columnVisibilitySwitchers);
    Object.keys(columnVisibilityMap).forEach(function (column) {
        if (columnVisibilityMap[column]) {
            columnSwitcher = columnVisibilitySwitchers.find(function (element) {
                return element.classList.contains(column)
            });
            columnSwitcher.classList.add('is-info');
        }
    });
}