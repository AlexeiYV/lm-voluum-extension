const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const ZipPlugin = require('zip-webpack-plugin');


module.exports = {
  entry: {
    'content': './src/content.js',
    'background': './src/background.js',
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.(png)$/,
        use: ['file-loader']
      }
    ]
  },
  mode: 'none',
  plugins: [
    new CleanWebpackPlugin(),
    new CopyPlugin([{
      from: './src/static'
    }]),
    new ZipPlugin({
      filename: 'extension.zip'
    })
  ]
}